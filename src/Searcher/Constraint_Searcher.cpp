#include "Constraint_Searcher.hpp"

const int Constraint_Searcher::DEFAULT_UNIFORM_STEPS = 20;
const int Constraint_Searcher::DEFAULT_BINARY_STEPS = 20;
unsigned Manifold_Piece::NEXT_CURVE_ID = 1;

double math_tools::func_distance(spline & f, spline & g, double a, double b, int slices){
    double max = 0;

    double x=a;

    for(int i=0; i<slices+1; i++, x+=(b-a)/slices){
        double d = fabs(f(x)-g(x));
        if(d > max) max = d;
    }

    return max;
}

inline double math_tools::lerp(double beg, double end, double cur, double tot){
    return beg + (cur/tot)*(end-beg);
}

inline double math_tools::lerp(double beg, double end, double weight){
    return beg + (weight)*(end-beg);
}

void Constraint_Searcher::solve_graph(){
    int realize = 
            Isostatic_Graph_Realizer::realize_2_tree_with_orientation(*graph, realization_order, _orientation);

}

void Constraint_Searcher::set_params(std::vector<double> params){
    for(int i=0; i<parameters.size(); i++){
        (*graph)[parameters[i].e_id].length = params[i];
    }
}

double Constraint_Curve::Curve_Piece::distance(Constraint_Curve::Curve_Piece & cp){
    return math_tools::func_distance(this->spl, cp.spl, 
        fmin(this->x[0], cp.x[0]), fmax(this->x[this->x.size()-1], cp.x[cp.x.size()-1]));
}



std::vector<double> Constraint_Curve::ys(int idx){
    Curve_Piece * cp = constraint_values[idx];
    return cp->y;
}

std::vector<int> Constraint_Searcher::orientation(){
    std::vector<int> temp;
    for(int i=0; i<_orientation.size(); i++)
        if(_orientation[i] == Oriented_Framework::orientation_types::ot_0)
            temp.push_back(0);
        else
            temp.push_back(1);
    return temp;
}

void Constraint_Searcher::orientation(std::vector<int> & ori){
    _orientation.clear();
    for(int i=0; i<ori.size(); i++)
        if(ori[i] == 0)
            _orientation.push_back(Oriented_Framework::orientation_types::ot_0);
        else
            _orientation.push_back(Oriented_Framework::orientation_types::ot_1);
}

std::vector<double> Constraint_Curve::critical_values(int idx){
    Curve_Piece * cp = constraint_values[idx];

    return cp->crit_values;
}

std::vector<double> Constraint_Curve::xs(int idx){
    Curve_Piece * cp = constraint_values[idx];
    return cp->x;
}

std::vector<std::vector<double> *> Constraint_Curve::ys(){
    std::vector<std::vector<double> *> temp;
    for(Curve_Piece * cp : constraint_values){
        temp.push_back(&cp->y);
        // for(double d : cp.y)
            // temp.back().insert(d);
    }

    return temp;
}

std::vector<std::vector<double> *> Constraint_Curve::critical_values(){
    std::vector<std::vector<double> *> temp;
    for(Curve_Piece * cp : constraint_values){
        temp.push_back(&cp->crit_values);
        // for(double d : cp.crit_values)
        //     temp.back().insert(d);
    }

    return temp;
}

std::vector<std::vector<double> *> Constraint_Curve::non_empty_critical_values(){
    std::vector<std::vector<double> *> temp;
    for(Curve_Piece * cp : constraint_values){
        if(!cp->crit_values.empty())
            temp.push_back(&cp->crit_values);
        // for(double d : cp.crit_values)
        //     temp.back().insert(d);
    }

    return temp;
}

std::vector<std::vector<double*> *> Constraint_Curve::lower_criticals(){
    std::vector<std::vector<double*> *> temp;
    for(Curve_Piece * cp : constraint_values){
        temp.push_back(&cp->lower_crit_params);
        // for(double d : cp.crit_values)
        //     temp.back().insert(d);
    }

    return temp;
}

std::vector<Constraint_Curve::Curve_ID> Constraint_Curve::non_empty_ids(){
    std::vector<Constraint_Curve::Curve_ID> temp;
    for(int i=0; i<constraint_values.size(); i++){
        if(!constraint_values[i]->crit_values.empty())
            temp.push_back(constraint_values[i]);
        // for(double d : cp.crit_values)
        //     temp.back().insert(d);
    }
}

std::vector<std::vector<double*> *> Constraint_Curve::non_empty_lower_criticals(){
    std::vector<std::vector<double*> *> temp;
    for(Curve_Piece * cp : constraint_values){
        if(!cp->lower_crit_params.empty())
            temp.push_back(&cp->lower_crit_params);
        // for(double d : cp.crit_values)
        //     temp.back().insert(d);
    }

    return temp;
}

std::vector<double> Constraint_Curve::all_critical_values(){
    std::vector<double> temp;
    for(Curve_Piece * cp : constraint_values){
        for(double d : cp->crit_values)
            temp.push_back(d);
        // for(double d : cp.crit_values)
        //     temp.back().insert(d);
    }

    return temp;
}

Constraint_Searcher::Constraint_Searcher(Graph * g) : graph(g){
    std::vector<Edge_ID> edges_to_remove;

    for(std::pair<Edge_Iterator, Edge_Iterator> es = graph->edges(); es.first != es.second; es.first++){
        if(!(*graph)[*(es.first)].type.compare("constraint")){
            Edge_ID e = *(es.first);
            std::pair<Vertex_ID, Vertex_ID> p = graph->vertices_incident(e);
            constraint_edges.push_back(p);
            constraint_values.push_back((*graph)[e].length);
            edges_to_remove.push_back(e);
        }
        else if(!(*graph)[*(es.first)].type.compare("parameter")){
            Edge_ID e = *(es.first);
            parameter_ids.push_back(e);
            UNIFORM_STEPS.push_back(DEFAULT_UNIFORM_STEPS);
            BINARY_STEPS.push_back(DEFAULT_BINARY_STEPS);
        }
    }

    for(Edge_ID e : edges_to_remove){
        graph->remove_edge(e);
    }

    realization_order = Isostatic_Graph_Realizer::realize_2_tree_order(*graph);

    init_sampling();
}

Constraint_Searcher::Constraint_Searcher(const char * filename){
    graph = new Graph();
    graph->read_from_file(filename);
    std::vector<Edge_ID> edges_to_remove;

    for(std::pair<Edge_Iterator, Edge_Iterator> es = graph->edges(); es.first != es.second; es.first++){
        if(!(*graph)[*(es.first)].type.compare("constraint")){
            Edge_ID e = *(es.first);
            std::pair<Vertex_ID, Vertex_ID> p = graph->vertices_incident(e);
            constraint_edges.push_back(p);
            constraint_values.push_back((*graph)[e].length);
            edges_to_remove.push_back(e);
        }
        else if(!(*graph)[*(es.first)].type.compare("parameter")){
            Edge_ID e = *(es.first);
            parameter_ids.push_back(e);
            UNIFORM_STEPS.push_back(DEFAULT_UNIFORM_STEPS);
            BINARY_STEPS.push_back(DEFAULT_BINARY_STEPS);
        }
    }

    graph->print_vertices();
    graph->print_edges();

    for(Edge_ID e : edges_to_remove){
        std::cout << e <<std::endl;
        graph->remove_edge(e);
    }

    realization_order = Isostatic_Graph_Realizer::realize_2_tree_order(*graph);

    //HARDCODING THE FLIP
    for(int i=0; i<realization_order.size()-2; i++){
        _orientation.push_back(Oriented_Framework::orientation_types::ot_1);
    }

    init_sampling();
}

Constraint_Searcher::Constraint_Searcher(Graph * g,
    std::vector<Edge_ID> & parameters, 
    std::vector<std::pair<Vertex_ID, Vertex_ID> > & constraints, 
    std::vector<float> & distances)
{

    graph = g;

    for(int i = 0; i < parameters.size(); i++){
        parameter_ids.push_back(parameters[i]);
        constraint_edges.push_back(constraints[i]);
        constraint_values.push_back(distances[i]);
    }

    realization_order = Isostatic_Graph_Realizer::realize_2_tree_order(*graph);
    init_sampling();
}

Constraint_Searcher::~Constraint_Searcher(){

}

std::vector<int> & Constraint_Searcher::binary(){
    return BINARY_STEPS;
}

std::vector<int> & Constraint_Searcher::uniform(){
    return UNIFORM_STEPS;
}

double Constraint_Searcher::epsilon(){
    return EPSILON;
}

void Constraint_Searcher::epsilon(float eps){
    if(eps > 0)
        EPSILON = eps;
}

void Constraint_Searcher::uniform(int uniform){
    UNIFORM_STEPS.clear();
    for(int i=0; i<parameters.size(); i++){
        UNIFORM_STEPS.push_back(uniform);
    }

    init_sampling();
}

void Constraint_Searcher::uniform(std::vector<int> & uniform){
    UNIFORM_STEPS.clear();
    for(int i=0; i<parameters.size(); i++){
        UNIFORM_STEPS.push_back(uniform[i]);
    }

    init_sampling();
}

void Constraint_Searcher::binary(int binary){
    BINARY_STEPS.clear();
    for(int i=0; i<parameters.size(); i++){
        BINARY_STEPS.push_back(binary);
    }

    init_sampling();
}

void Constraint_Searcher::binary(std::vector<int> & binary){
    BINARY_STEPS.clear();
    for(int i=0; i<parameters.size(); i++){
        BINARY_STEPS.push_back(binary[i]);
    }

    init_sampling();
}

void Constraint_Searcher::init_sampling(){
    parameters.clear();
    constraints.clear();

    //Make Constraints
    //NEEDS NUM_CONSTRAINTS = NUM_PARAMETERS
    for(int i=0; i<constraint_edges.size(); i++){
        constraints.push_back(Constraint(constraint_edges[i], constraint_values[i]));
    }

    std::cout << parameter_ids.size() << std::endl;
   
    for(int i=0;i<parameter_ids.size();i++){
    }

    for(int i=0; i<parameter_ids.size();i++){
        std::cout << parameter_ids[i] << std::endl;
        std::pair<double,double> temp = Isostatic_Graph_Realizer::interval_of_edge(*graph,parameter_ids[i]);
        std::cout << temp.first << " " << temp.second << std::endl;
        parameters.push_back(Parameter(parameter_ids[i], temp, UNIFORM_STEPS[i], BINARY_STEPS[i], &(constraints[i])));
    }

}

double Constraint_Searcher::_binary_search(Parameter_Iterartor p_it, double low, double high, double * lows, double * highs, int sign){
    Parameter & p = *p_it;
    Constraint & c = *p.paired_constraint;

    double c_length = c.distance;

    double mid = (low+high)/2.0;

    if(fabs(mid - high) < EPSILON/1000){
        std::cout << "BINARY SEARCH FAILED!" << std::endl;
        binary_fail = 1;
        graph->write_to_file("binary_fail.dot");
        delete [] highs;
        return mid;
    }

    (*graph)[p.e_id].length = mid;

    for(int i=0; i<current_depth; i++){
        (*graph)[(p_it+i+1)->e_id].length = 
            (lows[i]+highs[i])/2;

        std::cout << lows[i] << " " << highs[i] << std::endl;
    }

    int realize = 
            Isostatic_Graph_Realizer::realize_2_tree_with_orientation(*graph, realization_order, _orientation);

    if(!realize)
        std::cout << "Binary Failed" << std::endl;
    double x_dist = (*graph)[c.edge.first].x - (*graph)[c.edge.second].x;
    x_dist *= x_dist; 
    double y_dist = (*graph)[c.edge.first].y - (*graph)[c.edge.second].y;
    y_dist *= y_dist; 
    double d = sqrt(x_dist + y_dist);

    std::cout << "BINARY SEARCH: " << (mid-high) << std::endl;
    if(fabs(d - c_length) < EPSILON){
        Constraint_Curve * cc = curr_mani->c_curve.back();
        std::cout << "Pushing Critical" << std::endl;
        cc->current->crit_values.push_back(mid);
        for(int i=0; i<current_depth; i++)
            highs[i] = (lows[i] + highs[i])/2.0;

        if(current_depth)
            cc->current->lower_crit_params.push_back(highs);
        else
            cc->current->lower_crit_params.push_back(current_lower_params);
        return mid;
    }
    else if(sign*(d - c_length) > 0){
        for(int i=0; i<current_depth; i++)
            highs[i] = (lows[i] + highs[i])/2.0;
        return _binary_search(p_it, low, mid, lows, highs, sign);
    }
    else{
        for(int i=0; i<current_depth; i++)
            lows[i] = (lows[i] + highs[i])/2.0;
        return _binary_search(p_it, mid, high, lows, highs, sign);
    }

}

double Constraint_Searcher::fine_search(Parameter_Iterartor p_it, int sign)
{   
    Parameter & p = *p_it;
    double low = math_tools::lerp(p.crit_interval.first, p.crit_interval.second ,p.current_step-1,p.binary_steps);
    double high = math_tools::lerp(p.crit_interval.first, p.crit_interval.second ,p.current_step,p.binary_steps);

    double k;

    if(current_depth){
        double * lows = new double[current_depth];
        double * highs = new double[current_depth];

        for(int i=0; i<current_depth; i++){
            lows[i] = previous_lower_params[i];
            highs[i] = current_lower_params[i];
        }
       
        k = _binary_search(p_it, low, high, lows, highs, sign);
        delete [] lows;
    }
    else{
        k = _binary_search(p_it, low, high, NULL, NULL, sign);
    }            

    return k;
}

int Constraint_Searcher::realize_and_fine_search(
    Parameter_Iterartor p_it, Constraint & c,
    Constraint_Curve * cc, Constraint_Curve::Curve_ID cp, int & sign){

    // std::cout << cp->sign << " " << cp->id << cp->start << std::endl;
    //Realize the 2 tree
    int realize = 
            Isostatic_Graph_Realizer::realize_2_tree_with_orientation(*graph, realization_order, _orientation);
    // Mapped_Graph_Copy * g = 
    //     Isostatic_Graph_Realizer::realize_2_tree_with_orientation(graph, realization_order, orientation);

    // std::cout << cp->sign << " " << cp->id << cp->start << std::endl;
    int count = 0;
    Parameter & p = *p_it;
    double c_length = c.distance;


    double x_dist = (*graph)[c.edge.first].x - (*graph)[c.edge.second].x;
    x_dist *= x_dist; 
    double y_dist = (*graph)[c.edge.first].y - (*graph)[c.edge.second].y;
    y_dist *= y_dist; 
    double d = sqrt(x_dist + y_dist);
    
    cc->param_vals.push_back((*graph)[p.e_id].length);
    cp->y.push_back(d);
    cp->x.push_back((*graph)[p.e_id].length);

    std::cout << "     Dist:" << d << std::endl;

    //compare d and c_it.distance
    // std::cout << "     Sign, d-c, " << cp->sign << " " << (d-c_length) << std::endl;
    if(!cp->sign){
        cp->sign = (d > c_length ? 1 : -1);
    }
    if(cp->sign * (d - c_length) < 0){
        //SIGN CHANGE, found an equivalence, need to binary search using two previous points
        std::cout << "   Sign change found: " << std::endl;
        for(int i=0;i<parameters.size();i++)
            fprintf(stdout, "    P%-2i = %i\n", i, parameters[i].current_step);
        
        
        cp->sign *= -1;
        double actual_value = fine_search(p_it, cp->sign);
        

        if(binary_fail){
            binary_fail = 0;
            double low = math_tools::lerp(p.crit_interval.first, p.crit_interval.second ,p.current_step-1,p.binary_steps);
            (*graph)[p.e_id].length = low;
            Edge_Properties ep;
            ep.length = 0;
            ep.type = "constraint";
            Edge_ID e = graph->add_edge(c.edge.first, c.edge.second,
                ep);
            Isostatic_Graph_Realizer::realize_2_tree_with_orientation(*graph, realization_order, _orientation);
    
            double x_dist = (*graph)[c.edge.first].x - (*graph)[c.edge.second].x;
            x_dist *= x_dist; 
            double y_dist = (*graph)[c.edge.first].y - (*graph)[c.edge.second].y;
            y_dist *= y_dist; 
            double d = sqrt(x_dist + y_dist);

            (*graph)[e].length = d;
            graph->write_to_file("binary_before.dot");
            
            double high = math_tools::lerp(p.crit_interval.first, p.crit_interval.second ,p.current_step,p.binary_steps);
            (*graph)[p.e_id].length = high;
            Isostatic_Graph_Realizer::realize_2_tree_with_orientation(*graph, realization_order, _orientation);
            x_dist = (*graph)[c.edge.first].x - (*graph)[c.edge.second].x;
            x_dist *= x_dist; 
            y_dist = (*graph)[c.edge.first].y - (*graph)[c.edge.second].y;
            y_dist *= y_dist; 
            d = sqrt(x_dist + y_dist);
            (*graph)[e].length = d;

            graph->write_to_file("binary_after.dot");
        
            graph->remove_edge(e);
        }
        

        fprintf(stdout, "\n     Crit Param from %i to %i, val found is %2.5lf\n", p.current_step-1, p.current_step, actual_value);

        count++;
    }
    else{
        // if(current_lower_params) delete [] current_lower_params;
    }
    // current_lower_params = NULL;

    return count;
}

// void Constraint_Searcher::add_to_manifold()

int Constraint_Searcher::build_manifold_simple(Parameter_Iterartor p_it){
    Parameter & p = *p_it;
    Constraint & c = *p.paired_constraint;
    double c_length = c.distance;
    Constraint_Curve * cc = curr_mani->insert_curve(c_length);
    Constraint_Curve::Curve_ID cp = cc->start_piece(0);
    current_lower_params = NULL;



    std::cout << "   Manifold " << c_length << std::endl;
    int count = 0;
    int sign = 0;
    current_depth = 0;
    for(p.current_step = 0; p.current_step<p.binary_steps; p.current_step++){
        (*graph)[p.e_id].length = math_tools::lerp(p.interval.first, p.interval.second,p.current_step,p.binary_steps);
        count += realize_and_fine_search(p_it,c,cc,cp,sign);
    }

    cc->end_piece(cp, p.current_step);

    std::cout << "------" << count << "------" << std::endl;

    return count;
}

void Constraint_Searcher::find_crit_curve(Manifold_Piece * cc, int & begin, int & end){
    int beg_found = 0;
    for(int i=0; i<cc->c_curve.size(); i++){
        int num_crits = cc->c_curve[i]->all_critical_values().size();
        if(!beg_found && num_crits){
            beg_found = 1;
            begin = i;
        }
        else if(beg_found && !num_crits){
            end = i;
            return;
        }
    }
    end = cc->c_curve.size();
}

Constraint_Curve * Constraint_Searcher::get_lower_cc(Parameter_Iterartor p_it, int index){
    int depth = parameters.size() - curr_mani->dim - 1;

    Manifold_Piece * mp;
    if(p_it == parameters.begin())
        mp = curr_mani->pieces[0];
    else
        mp = curr_mani->pieces[std::prev(p_it)->current_step];

    Constraint_Curve * cc = mp->c_curve[index];


}

// void Constraint_Searcher::remove_singularities(Constraint_Curve * cc){
//     if(cc->constraint_values.size() == 1){
//         return;
//     } 
//     int last_num = 2;

//     for(int i = 0; i<cc->constraint_values.size(); i++){
//         Constraint_Curve::Curve_Piece * cp_i = cc->constraint_values[i];
//         double last = cp_i->y[0];
//         double slope = cp_i->y[1] - last;
//         slope /= cp_i->x[1] - cp_i->x[0];
//         // for(int j=0; j<last_num; j++){

//         // }
//         for(int j = 1; j<cp_i->y.size(); j++){
//             double new_slope = cp_i->y[i+1] - cp_i->y[i];
//             new_slope /= cp_i->x[i+1] - cp_i->x[i];

//         }
//     }

// }

void Constraint_Searcher::match_curves(){
    if(curr_mani->c_curve.size() == 1){
        Constraint_Curve * cc = curr_mani->c_curve[0];
        for(Constraint_Curve::Curve_Piece * cp : cc->constraint_values)
            cp->id = Manifold_Piece::curve_id();
        return;
    }

    Constraint_Curve * cc = curr_mani->c_curve[curr_mani->c_curve.size()-1];
    Constraint_Curve * back_cc = curr_mani->c_curve[curr_mani->c_curve.size()-2];

    //TODO: Make this faster... Try to do inline. I do not knwo how though

    if(cc->constraint_values.size() < back_cc->constraint_values.size()){
        std::vector<unsigned> used;
        for(Constraint_Curve::Curve_Piece * cp : cc->constraint_values){
            double min = 100000000;
            Constraint_Curve::Curve_Piece * the_one = NULL;
            for(Constraint_Curve::Curve_Piece * back_cp : back_cc->constraint_values){
                if(std::find(used.begin(), used.end(), back_cp->id) != used.end()) continue;
                double d = cp->distance(*back_cp);
                if(d < min){
                    min = d;
                    the_one = back_cp;
                }
            }

            if(the_one == NULL) std::cerr << "NO THE ONE!" << std::endl;
            cp->id = the_one->id;
            used.push_back(cp->id);
        }
    }
    else if(cc->constraint_values.size() > back_cc->constraint_values.size()){
        for(Constraint_Curve::Curve_Piece * back_cp : back_cc->constraint_values){
            double min = 100000000;
            Constraint_Curve::Curve_Piece * the_one = NULL;
            for(Constraint_Curve::Curve_Piece * cp : cc->constraint_values){
                if(cp->id != 0) continue;
                double d = cp->distance(*back_cp);
                if(d < min){
                    min = d;
                    the_one = cp;
                }
            }

            if(the_one == NULL) std::cerr << "NO THE ONE!" << std::endl;
            the_one->id = back_cp->id;
        }
        for(Constraint_Curve::Curve_Piece * cp : cc->constraint_values){
            if(cp->id == 0) cp->id = Manifold_Piece::curve_id();
        }
    }
    else{
        for(Constraint_Curve::Curve_Piece * back_cp : back_cc->constraint_values){
            double min = 100000000;
            Constraint_Curve::Curve_Piece * the_one = NULL;
            for(Constraint_Curve::Curve_Piece * cp : cc->constraint_values){
                if(cp->id != 0) continue;
                double d = cp->distance(*back_cp);
                if(d < min){
                    min = d;
                    the_one = cp;
                }
            }

            if(the_one == NULL) std::cerr << "NO THE ONE!" << std::endl;
            the_one->id = back_cp->id;
        }
        for(Constraint_Curve::Curve_Piece * cp : cc->constraint_values){
            if(cp->id == 0) cp->id = Manifold_Piece::curve_id();
        }
    }

}

void Constraint_Searcher::order_single_curve(
    std::vector<double> * big, std::vector<double> * little,
    std::vector<int> & order, std::vector<int> & missing_order,
    std::vector<Constraint_Curve::Curve_ID> & curve, int new_minus_old)
{

    int diff = big->size() - little->size();
    std::cout << "    Signle Diff: " << diff << " " << little->size() << std::endl;
    order.clear();
    missing_order.clear();

    std::vector<Constraint_Curve::Curve_ID> temp;
    
    int max_so_far = -1;
    int still_to_place = little->size();
    for(int j=0; j<little->size(); j++){
        double min = 100000;
        int min_index;
        for(int k=max_so_far+1; k<big->size()+1-still_to_place; k++){   
            double d = fabs((*(little))[j] - (*(big))[k]);
            std::cout << "     --- " << d << std::endl;
            if(d < min){
                min = d;
                min_index = k;
            }
        }
        max_so_far = min_index;
        still_to_place--;
        order.push_back(min_index);
    }


    for(int j=0; j<big->size(); j++){
        int found = 0;
        for(int k=0; k<little->size(); k++)
            if(order[k] == j) found = 1;
        if(!found) missing_order.push_back(j);
    }


    if(new_minus_old < 0){
        // BIG == OLD, reorder the curves so that new guys are up front
        // Means we do NOT have to index into the curves later with order
        for(int j=0; j<little->size(); j++)
            temp.push_back(curve[order[j]]);
        for(int j=0; j<diff; j++)
            temp.push_back(curve[missing_order[j]]);
        curve = temp;
    }
    else{
        // BIG == NEW, means we have started some new curves, which are now
        // in the back of the curve array. Old curves are still in front.
        // Go ahead and rearrange the array to match the new guys. Means we 
        // will have to index into curves using order later
        for(int j=0; j<little->size(); j++)
            temp.push_back(curve[order[j]]);
        for(int j=0; j<diff; j++)
            temp.push_back(curve[missing_order[j]]);
        curve = temp;
    }

    std::cout << "    O:";
    for(int i=0; i<order.size(); i++)
        std::cout << order[i] << " ";
    std::cout << std::endl;

    std::cout << "   MO:";
    for(int i=0; i<missing_order.size(); i++)
        std::cout << missing_order[i] << " ";
    std::cout << std::endl;
    // std::cout << curve.size() << temp.size() << std::endl;

    std::cout << "   CS:";
    for(int i=0; i<curve.size(); i++)
        std::cout << curve[i] << " ";
    std::cout << std::endl;
}

void Constraint_Searcher::order_all_curves(
    Constraint_Curve * cc,
    std::vector<std::vector<double> * > & more_curves,
    std::vector<std::vector<double> * > & less_curves,
    std::vector<int> & curves_order, std::vector<int> & curves_missing_order,
    int curves_diff, std::vector<std::vector<int> *> & orders,
    std::vector<std::vector<int> *> & missing_orders,
    std::vector<int> & needs_deleting, 
    std::vector<std::vector<Constraint_Curve::Curve_ID> * > & curves)
{

    //CURVES DIFF IS NEW - OLD

    for(int i=0; i<less_curves.size(); i++){
        std::vector<int> & order = *orders[i];
        std::vector<int> & missing_order = *missing_orders[i];
        int diff = less_curves[i]->size() - more_curves[curves_order[i]]->size();
        int diff_param = diff;
        if(curves_diff >= 0){
            // new == more
            // diff == old - new
            diff_param *= -1;
        }

        std::cout << "     Diff " << diff << std::endl;
        
        if(diff == 0) {
            if(needs_deleting[i]){
                //Delete the correct curves, last time, we had more curves in old
                std::vector<int>::reverse_iterator rit = order.rbegin();
                // for(; rit != order.rend(); ++rit){
                    std::cout << "     Deleting " << *(rit) << std::endl;
                    curves[i]->erase(curves[i]->begin() + order.size(),curves[i]->end());
                // }

                needs_deleting[i] = 0;
            }
            order_single_curve(less_curves[i],more_curves[curves_order[i]],
                order,missing_order,*curves[i],diff_param);
        }
        else if(diff > 0){
            if(curves_diff >= 0)
                needs_deleting[i] = 1;
            else
                curves[i]->push_back(cc->start_piece(0));
            
            order_single_curve(less_curves[i],more_curves[curves_order[i]],
                order,missing_order,*curves[i],diff_param);
        }
        else{
            if(curves_diff >= 0)
                curves[i]->push_back(cc->start_piece(0));
            else
                needs_deleting[i] = 1;
            order_single_curve(more_curves[curves_order[i]],less_curves[i],
                order,missing_order,*curves[i],-diff_param);
        }
    }

    for(int i=0; i<curves_missing_order.size(); i++){
        if(curves_diff < 0){
            //More low curves
            if(needs_deleting[i + less_curves.size()]){
                //Delete the correct curves, last time, we had more curves in old
                std::vector<int>::reverse_iterator rit = orders[i + less_curves.size()]->rbegin();
                // for(; rit != orders[i + less_curves.size()]->rend(); ++rit){
                    curves[i + less_curves.size()]->erase(
                        curves[i + less_curves.size()]->begin() + orders[i + less_curves.size()]->size(),
                        curves[i + less_curves.size()]->end());
                // }

                needs_deleting[i + less_curves.size()] = 0;
            }
        }
        else{
            //More high curves, need to push some pieces to setup the search..
            for(int j=0; j<more_curves[i + less_curves.size()]->size(); j++){
                curves[i+less_curves.size()]->push_back(cc->start_piece(0));
            }
        }



        //Need to order?

        // order_single_curve(less_curves[i],more_curves[curves_missing_order[i]],
        //     order,missing_order,curves[curves_missing_order[i]]);
    }
}

int Constraint_Searcher::search_single_curve(
    Parameter_Iterartor p_it, Constraint_Curve * cc, double weight,
    std::vector<double> * big, std::vector<double> * little,
    std::vector<double*> * big_vals, std::vector<double*> * little_vals,
    std::vector<int> & order, std::vector<int> & missing_order,
    std::vector<Constraint_Curve::Curve_ID> & curve, int new_minus_old){

    int count = 0;
    Constraint & c = *(p_it->paired_constraint);
    Constraint_Curve::Curve_ID cp;
    current_depth = parameters.size() - curr_mani->dim -1;
    int diff = big->size() - little->size();

    for(int j=0; j<little->size(); j++){
        if(new_minus_old <= 0)
            cp = curve[j];
        else
            cp = curve[order[j]];

        std::cout << "   " << j << " " << order[j] << " " << new_minus_old << " " << cp << " " << (*little)[j] << " " << (*big)[order[j]] << " " << weight << std::endl;
        cc->switch_curve(cp);
        current_lower_params = new double[current_depth];
        
        previous_lower_params = cp->old_lowers;

        current_lower_params[0] = math_tools::lerp((*little)[j], (*big)[order[j]], weight);

        for(int k=0; k<current_depth-1; k++){
            current_lower_params[k+1] = 
                 math_tools::lerp((*little_vals)[j][k], (*big_vals)[order[j]][k], weight);
        }
        
        for(int k=0; k<current_depth;k++){
            (*graph)[(p_it+k+1)->e_id].length = current_lower_params[k];
        }
        count += realize_and_fine_search(p_it,c,cc,cp,cp->sign);

        if(cp->old_lowers != NULL){
            delete [] cp->old_lowers;
        }

        cp->old_lowers = current_lower_params;
    }

    for(int j=0; j<diff; j++){
        if(new_minus_old <= 0)
            cp = curve[j + little->size()];
        else
            cp = curve[missing_order[j]];
        cc->switch_curve(cp);
        current_lower_params = new double[current_depth];
        previous_lower_params = cp->old_lowers;
        current_lower_params[0] = (*big)[missing_order[j]];

        for(int k=0; k<current_depth-1; k++){
            current_lower_params[k+1] = 
                (*big_vals)[missing_order[j]][k];
        }
        
        for(int k=0; k<current_depth;k++){
            (*graph)[(p_it+k+1)->e_id].length = current_lower_params[k];
        }
        count += realize_and_fine_search(p_it,c,cc,cp,cp->sign);

        if(cp->old_lowers != NULL){
            delete [] cp->old_lowers;
        }

        cp->old_lowers = current_lower_params;
    }


    return count;
}

int Constraint_Searcher::build_manifold_complicated(Parameter_Iterartor p_it){
    Parameter & p = *p_it;
    Constraint & c = *p.paired_constraint;
    double c_length = c.distance;
    Constraint_Curve * cc = curr_mani->insert_curve(c_length);
    std::vector<std::vector<Constraint_Curve::Curve_ID> *> curves;
    Constraint_Curve::Curve_ID cp;



    int crit_begin, crit_end;
    double crit_begin_length, crit_end_length;
    find_crit_curve(curr_mani->pieces[0], crit_begin, crit_end);

    crit_begin_length = math_tools::lerp(p.interval.first, p.interval.second, crit_begin,p.uniform_steps);
    crit_end_length = math_tools::lerp(p.interval.first, p.interval.second, crit_end,p.uniform_steps);

    p.crit_interval.first = crit_begin_length;
    p.crit_interval.second = crit_end_length;

    double crit_step = (p.interval.second-p.interval.first)/p.uniform_steps;
    double current_crit = crit_begin_length;
    double weight = 0;
    int current_crit_index = crit_begin;

    Constraint_Curve * old_cc = NULL;
    Constraint_Curve * new_cc = get_lower_cc(p_it, current_crit_index);

    std::vector<std::vector<double> * > old_crit_params;
    std::vector<std::vector<double> * > new_crit_params = new_cc->non_empty_critical_values();
    std::vector<std::vector<double*> *> old_lower_params;
    std::vector<std::vector<double*> *> new_lower_params = new_cc->non_empty_lower_criticals();
    

    std::vector<std::vector<int> *> orders;
    std::vector<std::vector<int> *> missing_orders;
    std::vector<int> needs_deleting;
    
    std::vector<int> curves_missing_order;
    std::vector<int> curves_order;
    int curves_needs_deleting = 0;

    for(int i=0; i<new_crit_params.size(); i++){
        curves.push_back(new std::vector<Constraint_Curve::Curve_ID>());
        orders.push_back(new std::vector<int>());
        missing_orders.push_back(new std::vector<int>());
        needs_deleting.push_back(0);
        std::cout << "low size " << new_crit_params[0]->size() << std::endl; 
        for(int j=0; j<new_crit_params[i]->size(); j++)
            curves[i]->push_back(cc->start_piece(0));
    }



    std::cout << "   Manifold " << c_length << " " << crit_begin << " " << crit_end << std::endl;
    int count = 0;


    for(p.current_step = 0; p.current_step<p.binary_steps; p.current_step++){
        //Set constraint
        double len = math_tools::lerp(crit_begin_length, crit_end_length,p.current_step,p.binary_steps);
        std::cout << "Steppin " << p.current_step << " " << len << std::endl;
        (*graph)[p.e_id].length = len;
        weight = (len - current_crit)/crit_step;
        
        if(old_cc == NULL || len > current_crit + crit_step){
            std::cout << "Crit Change" << current_crit_index << std::endl;
            if(old_cc != NULL) {
                current_crit_index++;
                current_crit += crit_step;
                weight = (len - current_crit)/crit_step;
            }
            
            old_cc = new_cc;
            old_crit_params = new_crit_params;
            old_lower_params = new_lower_params;
            if(current_crit_index < p.uniform_steps){
                new_cc = get_lower_cc(p_it, current_crit_index);
                new_crit_params = new_cc->non_empty_critical_values();
                new_lower_params = new_cc->non_empty_lower_criticals();
            }

            
            if(new_crit_params.size() > old_crit_params.size()){
                //Our lower c_curve has gained more pieces
                curves.push_back(new std::vector<Constraint_Curve::Curve_ID>());
                orders.push_back(new std::vector<int>());
                missing_orders.push_back(new std::vector<int>());
            }
            else if(new_crit_params.size() < old_crit_params.size()){
                // orders.pop_back();
                // missing_orders.pop_back();
                //c_curve has lost a piece

                curves_needs_deleting = 1;
            }
            else{
                if(curves_needs_deleting){
                    //TODO: sort missing order first
                    std::vector<int>::reverse_iterator rit = curves_order.rbegin();
                    for(; rit != curves_order.rend(); ++rit){
                        std::vector<Constraint_Curve::Curve_ID> * temp = (*(curves.begin() + (*rit)));
                        std::vector<int> * tempo = (*(orders.begin() + (*rit)));
                        std::vector<int> * tempmo = (*(missing_orders.begin() + (*rit)));

                        curves.erase(curves.begin() + (*rit));
                        orders.erase(orders.begin() + (*rit));
                        missing_orders.erase(missing_orders.begin() + (*rit));
                        needs_deleting.erase(needs_deleting.begin() + (*rit));

                        delete temp;
                        delete tempo;
                        delete tempmo;
                    }

                    curves_needs_deleting = 0;
                }
            }

            curves_order.clear();
            curves_missing_order.clear();

            std::vector<std::vector<double> * > & big = new_crit_params.size() >= old_crit_params.size() ? new_crit_params : old_crit_params;
            std::vector<std::vector<double> * > & little = new_crit_params.size() < old_crit_params.size() ? new_crit_params : old_crit_params;
            Constraint_Curve * big_cc = new_crit_params.size() >= old_crit_params.size() ? new_cc : old_cc;
            Constraint_Curve * little_cc = new_crit_params.size() < old_crit_params.size() ? new_cc : old_cc;

            std::vector<std::vector<Constraint_Curve::Curve_ID> * > temp;
            std::vector<std::vector<int> * > tempo;
            std::vector<std::vector<int> * > tempmo;
            std::vector<int > tempnd;

            
            for(int i=0; i<little.size(); i++){
                for(int j=0; j<big.size(); j++){
                    if(little_cc->constraint_values[i]->id == big_cc->constraint_values[j]->id)
                        curves_order.push_back(j);
                }
            }

            for(int i=0; i<big.size(); i++){
                int found = 0;
                for(int j=0; j<curves_order.size(); j++){
                    if(curves_order[j] == i){
                        found = 1;
                        break;
                    }
                }
                if(!found)
                    curves_missing_order.push_back(i);
            }

            for(int j=0; j<curves_order.size(); j++){
                temp.push_back(curves[curves_order[j]]);
                tempo.push_back(orders[curves_order[j]]);
                tempmo.push_back(missing_orders[curves_order[j]]);
                tempnd.push_back(needs_deleting[curves_order[j]]);
            }
            for(int j=0; j<curves_missing_order.size(); j++){
                temp.push_back(curves[curves_missing_order[j]]);
                tempo.push_back(orders[curves_missing_order[j]]);
                tempmo.push_back(missing_orders[curves_missing_order[j]]);
                tempnd.push_back(needs_deleting[curves_missing_order[j]]);
            }

            curves = temp;
            orders = tempo;
            missing_orders = tempmo;
            needs_deleting = tempnd;

            order_all_curves(cc,big,little,
                curves_order, curves_missing_order,
                new_crit_params.size()-old_crit_params.size(),
                orders,missing_orders,needs_deleting,curves);
        }
            //C curves assumed to be the same as before

        int nmo = new_crit_params.size() - old_crit_params.size();

        std::vector<std::vector<double> * > & big = nmo >= 0 ? new_crit_params : old_crit_params;
        std::vector<std::vector<double> * > & little = nmo < 0 ? new_crit_params : old_crit_params;
        std::vector<std::vector<double*> *> & big_lower = 
            nmo >= 0 ? new_lower_params : old_lower_params;
        std::vector<std::vector<double*> *> & little_lower = 
            nmo < 0 ? new_lower_params : old_lower_params;



        for(int i=0; i<curves_order.size(); i++){
            std::vector<int> & order = *orders[i];
            std::vector<int> & missing_order = *missing_orders[i];

            int diff = big[curves_order[i]]->size() - little[i]->size();
            int diffparam = nmo >= 0 ? diff : -diff;
            
            if(diff == 0){
                count += search_single_curve(p_it, cc, weight,
                    big[curves_order[i]],little[i],
                    big_lower[curves_order[i]], little_lower[i],
                    order, missing_order, *curves[i],
                    diff);
                
            }
            else if(diff > 0){
                count += search_single_curve(p_it, cc, weight,
                    big[curves_order[i]],little[i],
                    big_lower[curves_order[i]], little_lower[i],
                    order, missing_order, *curves[i],
                    diff);

            }
            else{
                count += search_single_curve(p_it, cc, 1-weight,
                    little[i], big[curves_order[i]],
                    little_lower[i], big_lower[curves_order[i]],
                    order, missing_order, *curves[i],
                    diff);
            }
        }

        for(int i=0; i<curves_missing_order.size(); i++){
            // Search with only one side of the curves....
            
            std::vector<Constraint_Curve::Curve_ID> & curve = *curves[i + curves_order.size()]; 

            int rem_depth = parameters.size() - curr_mani->dim - 1;

            int index = curves_missing_order[i];

            for(int j=0; j<big[index]->size(); j++){
                cp = curve[j];
                cc->switch_curve(cp);
                double * current_lower_params = new double[rem_depth];
                current_lower_params[0] = (*big[index])[j];
                std::vector<double *> * big_lowers = big_lower[index];
                for(int k=0; k<rem_depth-1; k++){
                    current_lower_params[k+1] = (*big_lowers)[j][k];
                }

                for(int k=0; k<rem_depth; k++)
                    (*graph)[(p_it+k+1)->e_id].length = current_lower_params[k];

                count += realize_and_fine_search(p_it, c, cc, cp, cp->sign);
            }

        }

    }

    //Compare found to the existing piece of manifold... 
    //Need some sort of map structure, or generalized graph

    // remove_singularities(cc);

    match_curves();

    std::cout << "------" << count << "------" << std::endl;

    return count;
}

int Constraint_Searcher::build_manifold_piece(Parameter_Iterartor  p_it)
{
    if(curr_mani->pieces.size() == 0)
        return build_manifold_simple(p_it);
    else 
        return build_manifold_complicated(p_it);
}


//TODO: templates?
int Constraint_Searcher::search_with_edge_set(Parameter_Iterartor p_it)
{
    Parameter & p = *p_it;
    p.current_step = 0;
    
    if((p_it+1) == parameters.end()){

        return build_manifold_piece(p_it);
    }

    int count = 0;
    curr_mani = curr_mani->insert_manifold();

    for(; p.current_step<p.uniform_steps; p.current_step++){
        std::cout << "Outer Parameter Change " << p.current_step << std::endl;
        (*graph)[p.e_id].length = math_tools::lerp(p.interval.first, p.interval.second,p.current_step,p.uniform_steps);
        count += search_with_edge_set(std::next(p_it,1));
    }

    curr_mani = curr_mani->parent;

    int t = build_manifold_piece(p_it);
    
    std::cout << "HEYO " << t << std::endl;

    std::cout << "------" << count << "------" << std::endl;
    
    for(int i=0;i<_orientation.size();i++)
        std::cout << _orientation[i];
    std::cout << std::endl;
    // if((p_it + 2) != parameters.end()){
    //     curr_mani = curr_mani->parent;
    // }
    return count;
}

int Constraint_Searcher::search(){
    if(curr_mani == NULL)
        delete curr_mani;

    curr_mani = new Manifold_Piece();
    top_mani = curr_mani;
    return search_with_edge_set(parameters.begin());
}

Constraint_Curve * Constraint_Searcher::get_Constraint_Curve(std::vector<int> leading){
    Manifold_Piece * mp = top_mani;
    if(leading.size() == 0)
        return top_mani->c_curve[0];
    mp = top_mani->pieces[0];
    for(int i=0; i<leading.size()-1; i++){
        mp = mp->pieces[i];
    }
    return mp->c_curve[leading[leading.size()-1]];
}