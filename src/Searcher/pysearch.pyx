from libcpp.vector cimport vector
from libcpp.string cimport string
from libcpp.pair cimport pair
from cython.operator cimport dereference as deref
import numpy as np
cimport numpy as np

ctypedef vector[double] * v_ptr

cdef extern from "Constraint_Searcher.hpp":
    cdef cppclass Constraint_Curve:
        Constraint_Curve() except +
        vector[double] ys(int)
        vector[double] xs(int)
        vector[double] critical_values(int)
        vector[double] all_critical_values()
        vector[v_ptr] ys()
        vector[double] xs()
        vector[v_ptr] critical_values()
        double constraint()
        int number_of_curves()

    cdef cppclass Constraint_Searcher:
        Constraint_Searcher(char *) except +
        int search()
        Constraint_Curve * get_Constraint_Curve(vector[int]);
        void epsilon(float)
        void uniform(int)
        void uniform(vector[int] &)
        void binary(int)
        void binary(vector[int] &)
        void orientation(vector[int] &)
        vector[int] orientation()
        double epsilon()
        vector[int] & uniform()
        vector[int] & binary()
        pair[double, double] get_param_interval(int)
        string graph_to_string()
        void set_params(vector[double])
        void solve_graph()


cdef class PyCurve:
    cdef Constraint_Curve * thisptr

    # def __cinit__(self):
    #     self.thisptr = new Constraint_Curve()

    # def __dealloc__(self):
        # delete self.thisptr

    def ys_index(self, i):
        cdef vector[double] temp = self.thisptr.ys(i)
        myarray = np.empty(temp.size())
        for i in range(temp.size()):
            myarray[i] = temp[i]
        return myarray

    def xs_index(self, i):
        cdef vector[double] temp = self.thisptr.xs(i)
        myarray = np.empty(temp.size())
        for i in range(temp.size()):
            myarray[i] = temp[i]
        return myarray

    def criticals_index(self, i):
        cdef vector[double] temp = self.thisptr.critical_values(i)
        myarray = np.empty(temp.size())
        for i in range(temp.size()):
            myarray[i] = temp[i]
        return myarray

    def xs(self):
        cdef vector[double] temp = self.thisptr.xs()
        myarray = np.empty(temp.size())
        for i in range(temp.size()):
            myarray[i] = temp[i]
        return myarray

    def ys(self):
        myarray = []
        for i in range(self.num_curves()):
            myarray.append(self.ys_index(i).tolist())
        return myarray

    def criticals(self):
        cdef vector[v_ptr] temp = self.thisptr.critical_values()
        myarray = []
        for i in range(temp.size()):
            temparry = []
            for j in range(temp[i].size()):
                t = temp[i]
                temparray += deref(temp[i])[j]
            myarray += temparray
        return myarray

    def all_criticals(self):
        cdef vector[double] temp = self.thisptr.all_critical_values()
        myarray = np.empty(temp.size())
        for i in range(temp.size()):
            myarray[i] = temp[i]
        return myarray

    def constraint(self):
        return self.thisptr.constraint()

    def num_curves(self):
        return self.thisptr.number_of_curves()


cdef create_pycurve(Constraint_Curve * cs):
    cdef PyCurve p = PyCurve()
    p.thisptr = cs
    return p

cdef class PySearcher:
    cdef Constraint_Searcher * thisptr

    def __cinit__(self, bytes s):
        self.thisptr = new Constraint_Searcher(s)

    def __dealloc__(self):
        del self.thisptr

    def search(self):
        return self.thisptr.search()

    def get_Constraint_Curve(self, list stuff):
        cdef vector[int] st = vector[int]()
        for i in range(len(stuff)):
            st.push_back(stuff[i])
        return create_pycurve(self.thisptr.get_Constraint_Curve(st))
            
    def epsilon(self, float f):
        self.thisptr.epsilon(f)

    def uniform(self, int i):
        self.thisptr.uniform(i)

    def uniform(self, list stuff):
        cdef vector[int] st = vector[int]()
        for i in range(len(stuff)):
            st.push_back(stuff[i])
        self.thisptr.uniform(st)

    def binary(self, int i):
        self.thisptr.binary(i)

    def binary(self, list stuff):
        cdef vector[int] st = vector[int]()
        for i in range(len(stuff)):
            st.push_back(stuff[i])
        self.thisptr.binary(st)

    def orientation(self, list stuff):
        cdef vector[int] st = vector[int]()
        for i in range(len(stuff)):
            st.push_back(stuff[i])
        self.thisptr.orientation(st)

    def get_epsilon(self):
        return self.thisptr.epsilon()

    def get_uniform(self):
        cdef vector[int] temp = self.thisptr.uniform()
        myarray = np.empty(temp.size())
        for i in range(temp.size()):
            myarray[i] = temp[i]
        return myarray

    def get_binary(self):
        cdef vector[int] temp = self.thisptr.binary()
        myarray = np.empty(temp.size())
        for i in range(temp.size()):
            myarray[i] = temp[i]
        return myarray

    def get_orientation(self):
        cdef vector[int] temp = self.thisptr.orientation()
        myarray = np.empty(temp.size())
        for i in range(temp.size()):
            myarray[i] = temp[i]
        return myarray

    def param_interval(self, i):
        cdef pair[double, double] t = self.thisptr.get_param_interval(i)
        return t.first, t.second

    def to_graphviz(self):
        return self.thisptr.graph_to_string()

    def set_params(self, list stuff):
        cdef vector[double] st = vector[double]()
        for i in range(len(stuff)):
            st.push_back(stuff[i])
        self.thisptr.set_params(st)

    def solve_graph(self):
        self.thisptr.solve_graph()

