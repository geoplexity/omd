import sys
from PyQt4 import QtGui
from PyQt4 import QtCore

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt
import matplotlib.animation as ani
import matplotlib.lines as lines
import scipy.interpolate as sp
import numpy as np
import settings
import networkx as nx


class Constraint_Window(QtGui.QDialog):
    def __init__(self, cs, parent=None):
        super(Constraint_Window, self).__init__(parent)

        self.cs = cs

        # a figure instance to plot on
        self.plot_figure = plt.figure()
        self.graph_figure = plt.figure()

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.plot_canvas = FigureCanvas(self.plot_figure)
        self.plot_canvas.setParent(self)
        # self.plot_canvas.mpl_connect('figure_enter_event', self.onpick)
        self.plot_canvas.mpl_connect('pick_event', self.onpick)
        # self.plot_canvas.mpl_connect('key_press_event', self.onpick)
        self.plot_canvas.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.plot_canvas.setFocus()
        self.graph_canvas = FigureCanvas(self.graph_figure)

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.plot_toolbar = NavigationToolbar(self.plot_canvas, self)
        # self.graph_toolbar = NavigationToolbar(self.graph_canvas, self)

        # Just some button connected to `plot` method
        self.button = QtGui.QPushButton('Search')
        self.button.clicked.connect(self.search)

        self.textbox = QtGui.QLineEdit('1111')

        splt1 = QtGui.QSplitter(QtCore.Qt.Vertical)

        overall = QtGui.QHBoxLayout()

        # set the layout
        f = QtGui.QFrame()
        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.plot_toolbar)
        layout.addWidget(self.plot_canvas)
        f.setLayout(layout)
        splt1.addWidget(f)
        # layout.addWidget(self.graph_toolbar)
        # layout.addWidget(self.graph_canvas)
        # layout.addStretch(1)
        f = QtGui.QFrame()
        hbox = QtGui.QHBoxLayout()
        hbox.addWidget(self.textbox)
        hbox.addWidget(self.button)
        f.setLayout(hbox)
        splt1.addWidget(f)

        overall.addWidget(splt1)
        self.setLayout(overall)

    def search(self):
        self.cs.orientation([int(i) for i in self.textbox.text()])
        self.cs.search()



        self.xss = []
        self.yss = []
        self.iss = []
        self.crits = []
        self.st, self.end = self.cs.param_interval(0)
        for i in range(settings.uniform):
            cc = self.cs.get_Constraint_Curve([i])
            temp = cc.all_criticals()
            if len(temp) != 0:
                self.iss += [self.st + (self.end - self.st)*i/settings.uniform] * len(temp)
                self.crits += temp.tolist()
            self.xss += [cc.xs()]
            self.yss += [cc.ys_index(0)]
        self.val = cc.constraint()

        self.cc = self.cs.get_Constraint_Curve([])

        self.pix, self.piy = self.cs.param_interval(1)

        self.plot2d()

        # self.xs = np.arange(self.st,self.end,(self.end-self.st)/settings.uniform)
        # self.ys = self.xss[0]
        # self.zs = self.yss
        # search_and_set([int(i) for i in self.textbox.toPlainText()])

    def plot2d(self):
        def run(i):
            ax.clear()
            line = ax.plot(xss[i], yss[i], marker='o', markersize=5.0)
            ax.plot([4,12],[val,val])
            ax.plot([4,12],[st + (end - st)*i/settings.uniform]*2)
            lag = sp.UnivariateSpline(xss[i], yss[i],s=0)
            xx = np.arange(xss[i][0], xss[i][-1], 0.1)
            ax.plot(xx, lag(xx))
            ax.plot(crits,iss, 'ro', picker=5.0)
            # ax.ylim([0, 16])

        fig = self.plot_figure
        cc = self.cc
        xss = self.xss
        st = self.st
        end = self.end
        yss = self.yss
        val = self.val
        pix = self.pix
        piy = self.piy
        iss = self.iss
        crits = self.crits
        
        # fig.clear()
        ax = fig.add_subplot(121)
        ax.clear()
        # ax.hold(False)
        for i in range(cc.num_curves()):
            ax.plot(cc.xs_index(i), cc.ys_index(i), marker='o', markersize=5.0,picker=5)

        ax.plot([cc.xs()[0],cc.xs()[-1]],[cc.constraint()]*2)

        ax = fig.add_subplot(122) 
        ax.clear()
        a = ani.FuncAnimation(fig, run, settings.uniform, interval=100)

        self.plot_canvas.draw()
        # plt.show()


    def onpick(self, event):

        # if event.artist != lines.Line2D:
        #     return True

        N = len(event.ind)
        if not N:
            return True

        # the click locations
        x = event.mouseevent.xdata
        y = event.mouseevent.ydata
        xs,ys = event.artist.get_data()

        distances = np.hypot(x - xs[event.ind], y - ys[event.ind])
        indmin = distances.argmin()
        dataind = event.ind[indmin]

        print event.ind
        print dataind
        print x, y
        # print x[dataind]
        print xs[dataind], ys[dataind]

        # self.lastind = dataind
        # self.update()
# if __name__ == '__main__':
#     app = QtGui.QApplication(sys.argv)

#     main = Window()
#     main.show()

#     sys.exit(app.exec_())