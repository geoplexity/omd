import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

os.environ["ETS_TOOLKIT"] = "qt4"


import numpy as np
from PyQt4 import QtGui
from QT_GUI import Constraint_Window
import settings
import matplotlib.pyplot as plt
import matplotlib.animation as ani
import scipy.interpolate as sp
from mpl_toolkits.mplot3d import axes3d
# import mayavi.mlab as mlab


import pysearch as ps

# def setup(cs):
#     xss = []
#     yss = []
#     iss = []
#     crits = []
#     st, end = cs.param_interval(0)
#     for i in range(uniform):
#         cc = cs.get_Constraint_Curve([i])
#         temp = cc.all_criticals()
#         if len(temp) != 0:
#             iss += [st + (end - st)*i/uniform] * len(temp)
#             crits += temp.tolist()
#         xss += [cc.xs()]
#         yss += [cc.ys_index(0)]
#     val = cc.constraint()
#     return xss, yss, val, iss, crits

# def plot3d():
#     s = mlab.contour_surf(xs,ys,zs,line_width=5,contours=[7.5])
#     s = mlab.surf(xs,ys,zs,opacity=0.7)
#     s2 = mlab.surf(xs,ys,[[val]*len(ys)]*len(xs),opacity=0.7)
#     mlab.axes(x_axis_visibility=True,y_axis_visibility=True,z_axis_visibility=True)
#     mlab.show()

# def plot2d():
#     def run(i):
#         ax.clear()
#         line = ax.plot(xss[i], yss[i], marker='o', markersize=5.0)
#         ax.plot([4,12],[val,val])
#         ax.plot([4,12],[st + (end - st)*i/uniform]*2)
#         lag = sp.UnivariateSpline(xss[i], yss[i],s=0)
#         xx = np.arange(xss[i][0], xss[i][-1], 0.1)
#         ax.plot(xx, lag(xx))
#         ax.plot(crits,iss, 'ro')
#         plt.ylim([0, 16])

#     for i in range(cc.num_curves()):
#         plt.plot(cc.xs_index(i), cc.ys_index(i), marker='o', markersize=5.0)

#     plt.plot([cc.xs()[0],cc.xs()[-1]],[cc.constraint()]*2)

#     fig, ax = plt.subplots()
#     a = ani.FuncAnimation(fig, run, uniform, interval=100)

#     plt.show()

# def search_and_set(ori):
#     global xss, yss, val, iss, crits
#     global ax, fig
#     global x_uppers, y_uppers
#     global cc
#     global st, end, pix, piy
#     global xs, ys, zs

#     cs.search()

#     xss, yss, val, iss, crits = setup(cs)
#     cc = cs.get_Constraint_Curve([])

#     st, end = cs.param_interval(0)
#     pix, piy = cs.param_interval(1)

#     xs = np.arange(st,end,(end-st)/uniform)
#     ys = xss[0]
#     zs = yss

if __name__ == '__main__':
    if len(sys.argv) > 1:
        cs = ps.PySearcher(sys.argv[1])
    else:
        cs = ps.PySearcher("../test_files/k33_constraints.dot")

    settings.init()

    cs.binary(settings.binary)
    cs.uniform(settings.uniform)
    app = QtGui.QApplication(sys.argv)

    main = Constraint_Window(cs)
    main.show()

    sys.exit(app.exec_())
    # a.save('demoanimation.gif', writer='imagemagick', );

    # ax.plot([1,3,2], [1,2,3])

    # plt.show()
