#include "Graph/Graph.hpp"
#include "Searcher/Constraint_Searcher.hpp"
#include "GUI/Constraint_Panel.hpp"

#include <iostream>
using namespace std;

enum runtime_options {
    ro_drp_2d = 0,
    ro_display_linkage = 1,
    ro_display_framework = 2,
    ro_omd = 3,
    ro_test = 4
};


namespace global {
    // Program *myProg;

    namespace dflt {
        string dot_file = "test_files/test.dot";
        runtime_options runtime_option = ro_drp_2d;
    }

    namespace window {
        const int width_screen_coords = 1000, height_screen_coords = width_screen_coords/1.618;
        // const int width_screen_coords = 1000, height_screen_coords = 1000;
        // int width_px = 1000, height_px = 1000;
        // const char *title = "Demo";
    }
}



int main(int argc, char **argv) {
    string dot_file = global::dflt::dot_file;
    runtime_options runtime_option = global::dflt::runtime_option;

    // handle command-line arguments
    for (int i = 1; i < argc; i++) {
        string inp = argv[i];
        string flag, val;

        int eqsign = inp.find("=");
        if (eqsign+1 == inp.length()) {
            cerr << "No value for flag " << inp.substr(0, eqsign) << "." << endl;
            return 0;
        } else if (eqsign == -1) {
            flag = inp;
            val = "";
        } else {
            flag = inp.substr(0, eqsign);
            val  = inp.substr(eqsign+1, inp.length());
        }

        // compare returns 0 if the value is the same, so not ! determines if they are the same
        if (!flag.compare("-dot")) {
            if (!val.compare("")) {
                i++;
                if (i == argc) {
                    cerr << "No filename provided for flag " << flag << "." << endl;
                    return 0;
                }
                dot_file = argv[i];
            } else {
                cerr << "Invalid value for flag " << flag << "." << endl;
                return 0;
            }
        }

        else if (!flag.compare("-opt")) {
            if (!val.compare("0")) {
                runtime_option = ro_drp_2d;
            } else if (!val.compare("1")) {
                runtime_option = ro_display_linkage;
            } else if (!val.compare("2")) {
                runtime_option = ro_display_framework;
            } else if (!val.compare("3")) {
                runtime_option = ro_omd;
            } else if (!val.compare("4")) {
                runtime_option = ro_test;
            } else {
                cerr << "Invalid value for flag " << flag << "." << endl;
                return 0;
            }
        }

        // else if (!flag.compare("-nogui")) {
        //     if (!val.compare("")) {
        //         nogui = true;
        //     } else {
        //         cerr << "Invalid value for flag " << flag << "." << endl;
        //         return 0;
        //     }
        // }

        else {
            cerr << "The flag " << flag << " is not recognized." << endl;
            return 0;
        }
    }


    // Graph g;
    // g.read_from_file(dot_file.c_str());
    // g.print_vertices();
    // g.print_edges();

    // Main_GUI_Manager * mgm = new Main_GUI_Manager();

    Constraint_Searcher * c = new Constraint_Searcher(dot_file.c_str());
    // Constraint_Panel * cv = new Constraint_Panel(mgm, c, std::vector<int>());
    
    // mgm->create_window(
    //     cv,
    //     global::window::width_screen_coords,
    //     global::window::height_screen_coords,
    //     "Constraint Stuff",
    //     3,
    //     2,
    //     true);


    // cv->init_drawer();
    // std::cout << "Hello" << std::endl;
    // cv->update_display();


    // while (!cv->should_close()) {
    //     // mgm.poll_for_events();
    //     // cout << "About to wait." << endl;
    //     mgm->wait_for_events();
    //     // cout << "Handled." << endl;
    // }
    c->search();

    std::vector<int> temp;
    temp.push_back(12);

    Constraint_Curve * cc = c->get_Constraint_Curve(temp);

    std::cout << cc->x.size() << std::endl;

    for(int i=0; i<cc->x.size(); i++)
        std::cout << cc->x[i] << ", " << cc->y[i] << std::endl;


    return 0;
}
